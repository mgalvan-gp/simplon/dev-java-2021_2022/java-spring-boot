package co.simplon.springcrud.entity;

import java.sql.Date;

public class Dog {
    private int id;
    private String name;
    private String breed;
    private Date createdAt;

    public Dog() {
    }

    public Dog(String name, String breed, Date createdAt) {
        this.name = name;
        this.breed = breed;
        this.createdAt = createdAt;
    }

    public Dog(int id, String name, String breed, Date createdAt) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
